fun is_older ( date1 : (int * int * int), date2 : (int * int * int) ) =
    if (#1 date1) < (#1 date2) then true
    else if (#1 date1) = (#1 date2) then if (#2 date1) < (#2 date2) then true
				    else if (#2 date1) = (#2 date2) then (#3 date1) < (#3 date2)
				    else false
    else false

	     
						  
	     
					  
fun number_in_month ( dates : (int * int * int) list, month : int) =
    if null dates then 0
    else if #2 (hd dates) = month then 1 + number_in_month ((tl dates), month)
    else
	number_in_month((tl dates), month)


fun number_in_months ( dates : (int * int * int) list, months : int list) =
    if null months orelse null dates then 0
    else number_in_month(dates, hd months) + number_in_months(dates, tl months)



fun dates_in_month (dates : (int * int * int) list, month: int) =
    if null dates then []
    else if #2 (hd dates) = month then (hd dates) :: dates_in_month((tl dates), month)
    else
	dates_in_month((tl dates), month)

fun dates_in_months (dates : (int * int * int) list , months : int list) =
    if null months orelse null dates then []
    else if #2 (hd dates) = (hd months) then dates_in_month(dates, (hd months)) @ dates_in_months(dates, (tl months))
    else dates_in_months(dates, (tl months))

fun get_nth (strs: string list, n : int) =
    if n <=0 orelse null strs then NONE
    else if n = 1 then SOME(hd strs)
    else get_nth(tl strs, n-1)
		
fun date_to_string (date : (int * int * int)) =
    let
	val months = ["January","February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    in
	valOf(get_nth(months,(#2 date))) ^ " " ^ Int.toString(#3 date) ^ "," ^ Int.toString(#1 date)
    end

fun get_nth_int (n :int, sum : int, int_list1 : int list) =
    if sum <= 0 then n
    else get_nth_int(n+1, (sum - (hd int_list1)),(tl int_list1))

fun number_before_reaching_sum (sum : int, num_list: int list) = get_nth_int(0,sum,num_list)


fun what_month ( day : int ) =
    let
	val days_in_each_month = [31,28,31,30,31,30,31,31,30,31,30,31];
    in
	number_before_reaching_sum ( day, days_in_each_month)
    end
	
fun month_range (day1 : int, day2 : int) =
    if day1 > day2 then []
    else if day1 = day2 then what_month(day2) :: []				     
    else what_month(day1) :: month_range(day1+1, day2)
					
fun oldest ( dates : (int * int * int) list) =
    if null dates then NONE
    else if length dates = 1 then SOME(hd dates)
    else
	let
	    val oldest_date = oldest(tl dates)
	in
	    if is_older (hd dates, valOf(oldest_date)) then SOME(hd dates)
	    else oldest_date
	end

fun contains (n : int, int_list : int list) =
    if null int_list then false
    else if n = (hd int_list) then true
    else contains(n, (tl int_list))
		 
fun remove_duplicates (int_list : int list) =
    if null int_list then []
    else if length int_list = 1 then int_list
    else if contains(hd int_list, tl int_list) then  remove_duplicates(tl int_list)
    else (hd int_list) :: remove_duplicates(tl int_list)

fun number_in_months_challenge (dates : (int * int * int) list, months : int list) =
    if null dates then 0
    else number_in_months(dates, remove_duplicates(months))
